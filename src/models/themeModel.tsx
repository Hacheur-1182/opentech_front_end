class ThemeModel {
  theme: string;
  constructor(theme: string) {
    this.theme = theme;
  }
}
export default ThemeModel;
