import React from "react";
import sliderModel from "../models/sliderModel";

const SliderImg: React.FC<{ img: sliderModel[] }> = (props) => {
  return (
    <>
      {props.img.map((img, index) => (
        <div key={index + img.alt} className="p-2">
          <img className={"img-slider"} src={img.img} alt={img.alt} />
        </div>
      ))}
    </>
  );
};

export default SliderImg;
