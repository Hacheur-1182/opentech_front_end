import sliderModel from "../models/sliderModel";

const sliderImgData = [
  new sliderModel("assets/img/DSC_7869.jpg", "img-slider"),
  new sliderModel("assets/img/DSC_7869.png", "img-slider"),
  new sliderModel("assets/img/DSC_7877.jpg", "img-slider"),
];

export default sliderImgData;
