import feedbackModel from "../models/feedbackModel";
import FeedbackModel from "../models/feedbackModel";

const feedbackInfo = [
  new feedbackModel(
    "Abdoul Abba",
    "Student",
    "Back school, I had a very theoretical understanding of how\n" +
      "                various aspects of the IoT stack worked. Training with the\n" +
      "                Inchtech's team allowed me to not only quickly learn the major\n" +
      "                aspects of various IoT systems, but it allowed me to physically\n" +
      "                work with various sensors and see the scope of this technology.\n" +
      "                Would highly recommend!",
    {
      borderIcon: "border-glass-2",
      textCardBody: "text-white",
      bg: "bg-dark",
    }
  ),
  new FeedbackModel(
    "John Doe",
    "Task Division",
    "I am new to the engineering field and i am basically a software engineer,\n" +
      "but the Inchtech's team taught me a complete range of hardware along\n" +
      "with software from top to bottom with very easy way to understand and\n" +
      "really with great effort. Thank you very much for the great work and self\n" +
      "interest of doing such great thing.",
    {
      borderIcon: "border-secondary",
      textCardBody: "",
      bg: "",
    }
  ),
];

export default feedbackInfo;
